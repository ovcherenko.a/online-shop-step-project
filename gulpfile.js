const gulp = require('gulp');                
const sass = require('gulp-sass');
const htmlPartial = require('gulp-html-partial');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const gulpCopy  = require("gulp-copy");
sass.compiler = require('node-sass');

gulp.task("copy-img", function() {
    return gulp
        .src(["./src/img/**/*.*"])
        .pipe(gulpCopy("build", { prefix: 1 }))
})

gulp.task("copy-partial-html", function() {
  return gulp
        .src(["./src/components/**/*.html"])
        .pipe(gulpCopy("partial-htmls", { prefix: 3 }))
});

gulp.task('sass', function () {
    return gulp.src([
        './src/**/*.scss',
        '!src/scss/custom-bootstrap.scss'
        ])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream())
});

gulp.task('sass:custom-bootstrap', function() {
    return gulp.src(['src/scss/custom-bootstrap.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream())
});

gulp.task('style:vendor', function () {
    return gulp
        .src([
            './node_modules/bootstrap-slider/dist/css/bootstrap-slider.css',
            './node_modules/slick-carousel/slick/slick.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('script', function(){
    return gulp.src([
        "./src/components/**/*.js",
        './src/scripts/*.js'
    ])
        .pipe(uglify())
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./build/js'))
        .pipe(browserSync.stream());
});

gulp.task('script:vendor', function () {
    return gulp
        .src([
                './node_modules/jquery/dist/jquery.js',
                './node_modules/popper.js/dist/umd/popper.min.js',
                './node_modules/bootstrap/dist/js/bootstrap.js',
                './node_modules/@fortawesome/fontawesome-free/js/all.js',
                './node_modules/bootstrap-slider/dist/bootstrap-slider.js',
                './node_modules/slick-carousel/slick/slick.js',
            ])
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('html', function () {
    return gulp.src(['./src/index.html'])
        .pipe(htmlPartial({
            basePath: './partial-htmls/'
        }))
        .pipe(gulp.dest('build'))
        .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });

    gulp.watch('./src/**/*.scss', gulp.series('sass')).on("change", browserSync.reload);
    gulp.watch('./src/scss/custom-bootstrap.scss', gulp.series('sass:custom-bootstrap')).on("change", browserSync.reload);
    gulp.watch('./src/**/*.html', gulp.series("copy-partial-html",'html')).on("change", browserSync.reload);
    gulp.watch('./src/**/*.js', gulp.series('script')).on("change", browserSync.reload);
});

// gulp.task('watch:html', function () {
//     gulp.watch('./src/**/*.html', gulp.series('html'));
// })

// gulp.task('watch:sass', function () {
//     gulp.watch('./src/**/*.scss', gulp.series('sass'));
// });

// gulp.task('watch:script', function () {
//     gulp.watch('./src/**/*.js', gulp.series('script'));
// });

// gulp.task('watch', gulp.parallel('watch:script', 'watch:sass', 'watch:html'));

gulp.task('default', gulp.series('style:vendor', "copy-img", "copy-partial-html", 'script:vendor','sass', 'sass:custom-bootstrap', 'script', 'html', 'browser-sync')
);