let furnGallery = $('#furniture-gallery-carousel-list .selected');
let furnGalleryInner = $('#furniture-gallery-carousel-list-inner');
let listGallery = $('#furniture-gallery-carousel-list');
let prevGallery = $('#furniture-gallery-carousel-prev');
let nextGallery = $('#furniture-gallery-carousel-next');

galleryLength = furnGallery.length * 84;
originalPosition = 0;

prevGallery.click(function() {
  console.log(originalPosition)
  if(originalPosition >= 1) {
    originalPosition = originalPosition - 84;
    furnGalleryInner.css('right', `${originalPosition}px`);
  }
})

nextGallery.click(function() {
  console.log(originalPosition)
  if(originalPosition < galleryLength - (84*6)) {
    originalPosition = originalPosition + 84;
    furnGalleryInner.css('right', `${originalPosition}px`)
  }
})