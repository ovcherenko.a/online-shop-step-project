$(document).ready(function () {
  $('.post-carousel').slick({
    dots: true,
    // autoplay: true,
    speed: 2000,
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    dotsClass: "slick-dots-custom-container",
    appendDots: $('.slider-controls-blog'),
    appendArrows: $(".slider-controls-blog"),
    prevArrow:"<div class='slick-arrow-blog slick-arrow-blog--left'><i class='fas fa-chevron-left'></i></div>",
    nextArrow:"<div class='slick-arrow-blog slick-arrow-blog--right'><i class='fas fa-chevron-right'></i></div>",
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 1000,
          arrows: true,
          dots: false
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
});