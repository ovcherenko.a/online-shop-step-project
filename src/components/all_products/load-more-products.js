let loadMoreButton = document.querySelector(".spinner-load-more");
loadMoreButton.addEventListener("click", ()=> {
  spin(loadMoreButton, 2)
});

function spin (element, degPerSec) {
  let deg=0;
  let spinInterval = setInterval(() => {
    element.setAttribute("style", `transform: rotate(${deg}deg)`);
    deg+=degPerSec;
  }, 10);
  setTimeout(()=> {
    clearInterval(spinInterval);
    element.classList.add("d-none");
    showCards();
  }, 2000)
}

function showCards () {
  console.log(document.querySelectorAll(".hidden-cards"));
  for (let el of document.querySelectorAll(".hidden-cards")) {
    el.classList.remove("d-none");
    el.classList.add("d-flex");
  }
}