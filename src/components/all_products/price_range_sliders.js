$("#price-range-slider1").slider({
  min: 135,
  max: 10000,
  step: 100,
  value: [135, 5000],
  tooltip: 'always',
  tooltip_position: 'top',
  range: true,
});

$("#price-range-slider2").slider({
  min: 135,
  max: 10000,
  step: 100,
  value: [135, 5000],
  tooltip: 'always',
  tooltip_position: 'top',
  range: true,
});